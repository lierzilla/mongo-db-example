1. Copy env.example to env and add DB_URI link
2. Use yarn to install 
3. Use yarn start

Result:

![alt text](https://i.ibb.co/SftNRfc/2024-01-21-07-08-59.png)
![alt text](https://i.ibb.co/rdmrPBN/2024-01-21-07-09-11.png)
