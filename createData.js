const { faker } = require('@faker-js/faker')
const { ObjectId } = require('mongodb')
function generateRandomAddress() {
  return {
    line1: faker.location.streetAddress(),
    line2: faker.location.secondaryAddress(),
    postcode: faker.location.zipCode(),
    city: faker.location.city(),
    state: faker.location.state({ abbreviated: true }),
    country: faker.location.countryCode(),
  }
}
function generateRandomCustomer() {
  return {
    _id: new ObjectId(),
    firstName: faker.person.firstName(),
    lastName: faker.person.lastName(),
    email: faker.internet.email(),
    address: generateRandomAddress(),
    createdAt: faker.date.past().toISOString(),
  }
}
function generateDocuments() {
  const numberOfCustomers = Math.floor(Math.random() * 10) + 1
  const customers = []
  for (let i = 0; i < numberOfCustomers; i++) {
    customers.push(generateRandomCustomer())
  }
  return customers
}

module.exports = { generateDocuments }
