const { ObjectId } = require('mongodb')
const { generateDocuments } = require('./createData')
function insertData(collection) {
  const customers = generateDocuments()
  collection.insertMany(customers, (err) => {
    if (err) {
      console.error('Error inserting documents:', err)
    }
  })
}
async function anonymizeDocument(document) {
  const anonymizedDocument = { ...document }

  anonymizedDocument.firstName = generateRandomString()
  anonymizedDocument.lastName = generateRandomString()
  anonymizedDocument.email = anonymizeEmail(anonymizedDocument.email)
  anonymizedDocument.address.line1 = generateRandomString()
  anonymizedDocument.address.line2 = generateRandomString()
  anonymizedDocument.address.postcode = generateRandomString()

  return anonymizedDocument
}

function anonymizeEmail(email) {
  const [domain] = email.split('@')
  const anonymizedUsername = generateRandomString()
  return `${anonymizedUsername}@${domain}`
}

function generateRandomString(length = 8) {
  let result = ''
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length)
    result += characters.charAt(randomIndex)
  }
  return result
}
async function anonymizeData(collection, anonymisedCollection) {
  const changeStream = collection.watch()

  changeStream.on('change', async (change) => {
    try {
      if (
        change.operationType === 'insert' ||
        change.operationType === 'update'
      ) {
        const updatedDocument = await collection.findOne({
          _id: new ObjectId(change.documentKey._id),
        })
        const anonymizedDocument = await anonymizeDocument(updatedDocument)
        await anonymisedCollection.insertOne(anonymizedDocument)
        console.log('Add new anonymised document')
      }
    } catch (error) {
      console.error('Error during document processing:', error)
    }
  })
}

module.exports = { insertData, anonymizeData }
