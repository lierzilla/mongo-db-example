require('dotenv').config()

const { MongoClient, ServerApiVersion } = require('mongodb')
const url = process.env.DB_URI
const client = new MongoClient(url, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
})

module.exports = {
  client,
}
