const { client } = require('./db')
const { insertData, anonymizeData } = require('./data')
async function init() {
  try {
    await client.connect()
    const db = client.db('shop')
    const collection = db.collection('customers')
    const anonymisedCollection = db.collection('customers_anonymised')

    setInterval(() => {
      insertData(collection)
    }, 200)

    await anonymizeData(collection, anonymisedCollection)
  } catch (error) {
    console.error('Error in init:', error)
  } finally {
    if (client && client.isConnected) {
      await client.close()
    }
  }
}

init()
